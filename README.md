maven 环境配置插件
用法：

pom.xml导入插件。
```
            <plugin>
                <groupId>com.forg</groupId>
                <artifactId>forgMavenConfig</artifactId>
                <version>1.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>replaceConfig</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
```
            
执行替换            
```
mvn clean package -Denv=test
```

test为指定env环境目录,由env属性指定,默认放置 路径为"源码"目录平级的 conf/目录下
conf/ 地址可以由 conf 属性指定. 默认覆盖/新增   war/jar包下的"WEB-INF/classes/" 可由targetDir属性指定。

使用env目录中所有配置文件 复制/覆盖 到目标地址.

默认目录结构事例：
```
......
src/
conf/
    dev/
        ......
    test/
        ......
    www/
        ......
......
```